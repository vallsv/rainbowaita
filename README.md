# Rainbowaita Cursor Theme

![Rainbowaita Icons](src/cursors.svg)

This is a derivative theme from [Adwaita Icon Theme](https://github.com/GNOME/adwaita-icon-theme) from the GNOME project.

It uses concepts from [Posy's Cursors](http://www.michieldb.nl/other/cursors/).

# Changes

| Adwaita | Rainbowaita  | Description |
|:-------:|:-----------:|-------------|
| ![icon](./doc/adwaita/left_ptr.png) | ![icon](./doc/rainbowaita/left_ptr.png) | Remove tail from the default cursor |
| ![icon](./doc/adwaita/watch_0001.png) | ![icon](./doc/rainbowaita/watch_0016.png) | Use animated hourglass concept from Posy's Cursors for the wait cursor |
| ![icon](./doc/adwaita/left_ptr_watch_0001.png) | ![icon](./doc/rainbowaita/left_ptr_watch_0010.png) | Use animated hourglass concept from Posy's Cursors for the progress cursor |
| ![icon](./doc/adwaita/question_arrow.png) | ![icon](./doc/rainbowaita/question_arrow.png) | Use a default pointer for the question cursor |
| ![icon](./doc/adwaita/hand1.png) | ![icon](./doc/rainbowaita/hand1.png) | Remove white detail from the hand cursors |
| ![icon](./doc/adwaita/context-menu.png) | ![icon](./doc/rainbowaita/context-menu.png) | Tune color of the context-menu cursor|
| ![icon](./doc/adwaita/pencil.png) | ![icon](./doc/rainbowaita/pencil.png) | Tune color of the pencil cursor |

# Compile, package, install

It can be installed in Linux desktop the following way:

```
./make.py compile package install
```

# Manual installation

Without compilation, the package can be installed
the following way:

```
cp -r ./package/* ~/.icons/
```

Or system wide
```
sudo cp -r ./package/* /usr/share/icons/
```
