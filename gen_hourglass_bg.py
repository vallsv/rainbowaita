#!/usr/bin/env python3

"""
Generate hourglass background based on an intensity map
and a set of shifted color look up tables.
"""

import sys
import numpy
import skimage.io
import subprocess

def read_image(filename):
    array = skimage.io.imread(filename)
    print(array.shape)
    if len(array.shape) == 3:
        array = array[:, :, 0]
    return array / 255

def rgba(color):
    assert len(color) in (7, 9) and color[0] == '#'
    r = int(color[1:3], 16) / 255
    g = int(color[3:5], 16) / 255
    b = int(color[5:7], 16) / 255
    a = int(color[7:9], 16) / 255 if len(color) == 9 else 1.
    return r, g, b, a

colors = [
    "#c000ff",
    "#0066ff",
    "#00baff",
    "#46f609",
    "#fffc00",
    "#fea002",
    "#ff0030",
]
colors = [rgba(c) for c in colors]


def create_colormap(id, nb):
    delta = int((id * 256) / nb)
    colormap = []
    for i in range(0, 256):
        i = (i + 256 - delta) % 256
        i = int(i * (len(colors)) / 255)
        c = colors[i % len(colors)]
        colormap.append(c)
    colormap = numpy.array(colormap)
    return colormap


def create_colormaps(nb_colormaps):
    colormaps = []
    for i in range(nb_colormaps):
        c = create_colormap(i, nb_colormaps)
        colormaps.append(c)
    return colormaps


# Create a LUT with shifted colors, one for each animation
colormaps = create_colormaps(32)
colormap_id = 0

mask = read_image("src/hourglass-mask.png")
mask[mask < 0.5] = numpy.nan
mask[mask >= 0.5] = 1

# Get the hourglass width at different height
vprofile = numpy.nansum(mask, axis=1) / 2
vprofile[vprofile == 0] = numpy.nan
print("vprofile", vprofile.shape)

# Compute a radial hourglass SDF
radial = numpy.empty(mask.shape)
radial[:] = 1 - 2 * numpy.arange(0, radial.shape[1]) / radial.shape[1]
for y in range(radial.shape[0]):
    v = vprofile[y]
    if not numpy.isnan(v):
        x = radial.shape[1] // 2 + int(v) - 1
        v = radial[y, x]
    radial[y, :] *= 1 / v

# Compute a vertical hourglass SDF
def create_vprofile_sdf(vprofile):
    sdf = numpy.array(vprofile)
    sdf = sdf - numpy.nanmin(sdf) 
    sdf = sdf / numpy.nanmax(sdf)
    sdf[sdf.shape[0]//2:] = -sdf[sdf.shape[0]//2:]
    print("vprofile SDF", numpy.nanmin(sdf), numpy.nanmax(sdf))
    return sdf

vprofile_sdf = create_vprofile_sdf(vprofile)
vert = numpy.empty(mask.shape)
for x in range(vert.shape[1]):
    vert[:, x] = vprofile_sdf

hlin = numpy.empty(mask.shape)
base = numpy.linspace(-1, 1, num=radial.shape[0])
for x in range(hlin.shape[1]):
    hlin[:, x] = base

vlin = numpy.empty(mask.shape)
base = numpy.linspace(-1, 1, num=radial.shape[1])
for y in range(vlin.shape[0]):
    vlin[y, :] = base


array = radial * vert * mask
array = numpy.exp(radial) * vert * mask
#array = numpy.exp(radial/10) *  mask

# Diagonal
array = (vlin+hlin)*0.5 * mask

array = (radial+-1*vert)*0.5 * mask

array = ((hlin*1.2+radial+-1*vert)) / 8 * mask


def compile_hourglass():
    dirname = "src/hourglass"
    subprocess.run(["mkdir", "-p", dirname])
    print("Array min/max", numpy.nanmin(array), numpy.nanmax(array))
    arrayb = (array * 255).astype(numpy.uint8)
    arrayb = numpy.flip(arrayb, axis=0)
    for i, lut in enumerate(colormaps):
        lut = (lut * 255).astype(numpy.uint8)
        print(f"Compile hourglass LUT {i}")
        a = lut[arrayb]
        a[numpy.isnan(mask),:] = [0, 0, 0, 0]
        filename = f"{dirname}/hourglass_{i:02d}.png"
        skimage.io.imsave(filename, a)

compile_hourglass()


def gui():
    from silx.gui import qt
    from silx.gui.plot import ImageView
    from silx.gui.plot import Plot1D
    from silx.gui.plot import Plot2D
    from silx.gui import colors as silx_colors

    app = qt.QApplication([])

    if False:
        plot = Plot2D()
        plot.addImage(array)
        plot.show()

    if True:
        plot = ImageView()
        plot.addImage(array)
        plot.show()

    if False:
        plot = Plot1D()
        plot.addCurve(x=numpy.arange(vprofile.shape[0]), y=vprofile, legend="vprofile")
        plot.addCurve(x=numpy.arange(vprofile_sdf.shape[0]), y=vprofile_sdf, legend="sdfprofile")
        plot.show()

    colormaps = [silx_colors.Colormap(colors=c) for c in colormaps]

    timer = qt.QTimer()
    def update_colormap():
        global colormap_id
        colormap_id = (colormap_id+1) % len(colormaps)
        plot.setColormap(colormaps[colormap_id])
    timer.timeout.connect(update_colormap)
    if isinstance(plot, ImageView):
        timer.start(50)

    app.exec()

#gui()