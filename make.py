#!/usr/bin/env python3

import sys
import os
import toml
import argparse
import subprocess
import glob
import getpass
from typing import List, NamedTuple
from functools import lru_cache

try:
    from lxml import etree
except ImportError:
    import xml.etree.ElementTree as etree

CURSOR_THEME = """\
[Icon Theme]
Name={Name}
Comment={Comment}
Example={Example}
Inherits={Inherits}
"""

BUILD_DIR = "build"
SRC_DIR = "src"
GOALS = {}


def goal(func):
    """Decorator registring a function as an available goal."""
    name = func.__name__.replace("_", ":")
    def func2(*args, **kwargs):
        nonlocal func, name
        print(f"Goal {name}")
        func(*args, **kwargs)
    GOALS[name] = func2
    func2.__name__ = func.__name__
    func2.__doc__ = func.__doc__
    return func2


class Config:
    def __init__(self, root):
        with open(f"{root}/cursors.toml", "rt") as f:
            self.__content = toml.load(f)
        tree = etree.parse(f"{root}/src/cursors.svg")
        self.__root = tree.getroot()

    @property
    def content(self):
        return self.__content

    @lru_cache
    def get_cursor_sizes(self):
        return self.__content["cursors"]["sizes"]

    @lru_cache
    def get_cursor_names(self):
        cursors = self.__content["cursor"]
        names = [n for n, v in cursors.items() if isinstance(v, dict)]
        return names

    @lru_cache
    def get_cursor_info(self, name):
        cursors = self.__content["cursor"]
        return cursors[name]

    @lru_cache
    def get_cursor_nb_animation(self, name):
        return self.get_cursor_info(name).get("nb_animation", 0)

    @lru_cache
    def get_cursor_duration(self, name):
        return self.get_cursor_info(name).get("duration", 0)

    @lru_cache
    def get_raw_pointer_location(self, name):
        """Get the pointer location as stored in SVG file.

        It is normalized to 24px.
        """
        rect = self.__root.find(f".//{{http://www.w3.org/2000/svg}}rect[@id='{name}']")
        circle = self.__root.find(
            f".//{{http://www.w3.org/2000/svg}}circle[@id='{name}__ptr']"
        )
        orig_x = float(rect.get("x"))
        orig_y = float(rect.get("y"))
        pos_x = float(circle.get("cx"))
        pos_y = float(circle.get("cy"))
        return pos_x - orig_x, pos_y - orig_y

    def get_pointer_location(self, name, size):
        """
        Get the pointer location from a specific cursor (name and size).
        """
        nb_animation = self.get_cursor_nb_animation(name)
        if nb_animation > 0:
            name = f"{name}_0001"
        x, y = self.get_raw_pointer_location(name)
        return int(x * size / 24), int(y * size / 24)

    def get_cursor_indexes(self, name):
        nb_animation = self.get_cursor_nb_animation(name)
        if nb_animation:
            indexes = range(1, nb_animation + 1)
            return [f"_{i:04d}" for i in indexes]
        else:
            return [""]


def if_outdated(source, target):
    """Decorator to call a function only if source and target file are outdated"""

    def inner(func):
        def call_if_outdated(*args, **kwargs):
            nonlocal source, target
            sname = source.format(**kwargs)
            tname = target.format(**kwargs)
            valid = "{" not in sname and "{" not in tname
            if valid:
                if not os.path.exists(sname):
                    print(f"{sname} -> {tname} [missing source]")
                    return
                if os.path.exists(tname):
                    stime = os.path.getmtime(sname)
                    ttime = os.path.getmtime(tname)
                    if stime < ttime:
                        print(f"{sname} -> {tname} [up-to-date]")
                        return
            if valid:
                print(f"{sname} -> {tname}")
            func(*args, source=sname, target=tname, **kwargs)
            if valid:
                print(f"{sname} -> {tname} [done]")

        return call_if_outdated

    return inner


class Context(NamedTuple):
    """Context of the application"""

    config: Config
    options: NamedTuple
    cursor_selection: List[str]


@if_outdated(source="src/cursors.svg", target="build/{size}x{size}/{name}{index}.png")
def create_png_file(context, size, name, index, source, target):
    args = [
        "inkscape",
        source,
        f"--export-id={name}{index}",
        f"--export-filename={target}",
        f"--export-width={size}",
        f"--export-height={size}",
    ]
    subprocess.run(args)


@goal
def compile_png(context: Context):
    """Compile png files from the source"""
    config = context.config
    subprocess.run(["mkdir", "-p", BUILD_DIR])
    for size in config.get_cursor_sizes():
        dirname = f"{BUILD_DIR}/{size}x{size}"
        subprocess.run(["mkdir", "-p", dirname])

    for name in context.cursor_selection:
        indexes = config.get_cursor_indexes(name)
        for index in indexes:
            for size in config.get_cursor_sizes():
                create_png_file(config, size=size, name=name, index=index)


@if_outdated(source="src/cursors.svg", target="build/{name}.xcg")
def create_xcg_file(context: Context, name, source, target):
    config = context.config
    subprocess.run(["mkdir", "-p", BUILD_DIR])
    nb_animation = config.get_cursor_nb_animation(name)
    if nb_animation:
        delay = int(config.get_cursor_duration(name) * 1000 / nb_animation)
        delay = f" {delay}"
    else:
        delay = None
    animation_filter = "" if nb_animation == 0 else "_????"
    with open(target, "wt") as xcg:
        for size in config.get_cursor_sizes():
            pointer = config.get_pointer_location(name, size)
            # FIXME: there is no need of glob, the config is enough
            pngs = glob.glob(f"{BUILD_DIR}/{size}x{size}/{name}{animation_filter}.png")
            pngs = sorted(pngs)
            for png in pngs:
                xcg.write(f"{size} {pointer[0]} {pointer[1]} {png}")
                if delay:
                    xcg.write(f" {delay}")
                xcg.write("\n")


@goal
def compile_xcg(context: Context):
    """Compile xcg files from the source"""
    for name in context.cursor_selection:
        create_xcg_file(context, name=name)


@goal
def compile_cursor(context: Context):
    """Compile cursor files from intermediate png+xcg"""
    for name in context.cursor_selection:
        cursors_dir = f"{BUILD_DIR}/theme/cursors"
        subprocess.run(["mkdir", "-p", f"{cursors_dir}"])
        args = ["xcursorgen", f"{BUILD_DIR}/{name}.xcg", f"{cursors_dir}/{name}"]
        subprocess.run(args)


@goal
def compile_link(context: Context):
    """Compile cursor aliases as symlink from source"""
    for name in context.cursor_selection:
        info = context.config.get_cursor_info(name)
        cursors_dir = f"{BUILD_DIR}/theme/cursors"
        for alias in info.get("aliases", []):
            subprocess.run(["ln", "-s", f"{name}", f"{cursors_dir}/{alias}"])


@goal
def compile_theme(context: Context):
    """Compile theme description from the source"""
    output = f"{BUILD_DIR}/theme/"
    subprocess.run(["mkdir", "-p", output])
    info = context.config.content["theme"]
    with open(f"{output}/index.theme", "wt") as f:
        f.write(CURSOR_THEME.format(**info))


@goal
def compile(context: Context):
    """Compile all expected files from the source"""
    for goal, callable_goal in GOALS.items():
        if not goal.startswith(f"{compile.__name__}:"):
            continue
        callable_goal(context)


@goal
def package(context: Context):
    """Package the theme into a valid structure"""
    info = context.config.content["theme"]
    name = info["Name"]
    subprocess.run(["rm", "-rf", "./package"])
    subprocess.run(["mkdir", "-p", "./package"])
    subprocess.run(["cp", "-r", "./build/theme", f"./package/{name}"])


@goal
def install_system(context: Context):
    """Install the package system wide"""
    info = context.config.content["theme"]
    name = info["Name"]
    subprocess.run(["cp", "-r", f"./package/{name}", "/usr/share/icons"])


@goal
def install_user(context: Context):
    """Install the package into the user account.

    Use `~/.icons/` as target.

    Sounds like `~/.local/share/icons` is not properly handled
    with Debian 10, Gnome Shell.
    """
    info = context.config.content["theme"]
    name = info["Name"]
    dirname = f"{os.path.expanduser('~')}/.icons"
    subprocess.run(["mkdir", "-p", dirname])
    subprocess.run(["cp", "-rf", f"./package/{name}", dirname])


@goal
def install(context: Context):
    """Install the package system wide if possible else in user account"""
    if getpass.getuser() == "root":
        install_system(context)
    else:
        install_user(context)


@goal
def clean_xcg(context: Context):
    """Clean every XCG files from the build"""
    subprocess.run(" ".join(["rm", "./build/*.xcg"]), shell=True)


@goal
def clean_cursor(context: Context):
    """Clean only specific cursors from the build"""
    config = context.config
    for name in context.cursor_selection:
        subprocess.run(["rm", f"./build/{name}.xcg"])
        indexes = config.get_cursor_indexes(name)
        for size in config.get_cursor_sizes():
            dirname = f"./build/{size}x{size}"
            for index in indexes:
                subprocess.run(["rm", f"{dirname}/{name}{index}.png"])

@goal
def clean(context: Context):
    """Clean build and package directories"""
    subprocess.run(["rm", "-r", "./package"])
    subprocess.run(["rm", "-r", "./build"])


def str_goal(string):
    string = str(string)
    if string not in GOALS:
        raise ValueError("'{string}' is not a goal")
    return string


def main():
    parser = argparse.ArgumentParser(description="Make cursor theme from source.")
    parser.add_argument(
        "goals",
        type=str_goal,
        nargs="+",
        help=f"List of goals. One of {', '.join(GOALS.keys())}",
    )
    parser.add_argument(
        "--cursors",
        type=lambda s: s.split(","),
        default=["all"],
        help="List of cursors",
    )
    options = parser.parse_args()

    config = Config(".")
    cursors = options.cursors
    if cursors == ["all"]:
        cursors = config.get_cursor_names()

    context = Context(config=config, options=options, cursor_selection=cursors)

    for goal in options.goals:
        callable_goal = GOALS[goal]
        callable_goal(context)


if __name__ == "__main__":
    main()
